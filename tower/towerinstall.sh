echo "##################################"
echo "installing EPEL repo"
echo "##################################"
yum install -y http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
echo "##################################"
echo "installing opensource Ansible as it will be used to install Tower"
echo "##################################"
yum install -y ansible
echo "##################################"
echo "Ansible is installed. Now downloading Tower Setup Files"
echo "##################################"
wget https://releases.ansible.com/ansible-tower/setup/ansible-tower-setup-latest.tar.gz
echo "##################################"
echo "Tower Setup files downloaded. Extracting the installer...."
echo "##################################"
tar xvzf ansible-tower-setup-latest.tar.gz
cd ansible-tower-setup-*
echo "##################################"
echo "Here are the files extracted"
echo "##################################"
pwd
ls
echo "##################################"
echo "Replacing Inventory file for Single Node Setup"
echo "##################################"
mv inventory inv.bkp
wget https://gitlab.com/roybhaskar9/ansible/raw/master/tower/inventory
echo "##################################"
echo "Running the setup"
echo "##################################"
sh setup.sh
